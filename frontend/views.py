from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.conf import settings

import os
import requests

api_url = os.getenv('API_URL')
client_id = os.getenv('CLIENT_ID')
client_secret = os.getenv('CLIENT_SECRET')

def homepage(response):
	context = {}
	return render(response, 'frontend/homepage.html', context)

def user_login(response):
	context = {}
	return render(response, 'frontend/login.html', context)

def authenticate(response):
	username = response.POST.get('username')
	password = response.POST.get('password')
	token_request = generate_token(username, password)
	return HttpResponse(token_request.get('access_token'))

def user_signup(response):
	context = {}
	return render(response, 'frontend/signup.html', context)

def edit_profile(response):
	context = {}
	return render(response, 'frontend/edit_profile.html', context)

def edit_password(response):
	context = {}
	return render(response, 'frontend/edit_password.html', context)

def family_tree_view(response):
	context = {}
	return render(response, 'frontend/family_tree_view.html', context)

def edit_member(response, member_id):
	context = {'member_id': member_id}
	return render(response, 'frontend/edit_member.html', context)

def add_member(response):
	context = {}
	return render(response, 'frontend/add_member.html', context)

def generate_token(username, password):
	url = "{}/o/token/".format(api_url)
	payload = {
		'grant_type': 'password',
		'username': username,
		'password': password
	}

	auth = (client_id, client_secret)
	resource = requests.post(url, data=payload, auth=auth)
	resource_data = resource.json()

	return resource_data
