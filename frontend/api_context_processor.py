import os 

def export_vars(request):
    data = {}
    data['api_endpoint'] = os.getenv('API_URL')
    return data