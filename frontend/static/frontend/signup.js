let signupEndpoint = apiEndpoint + '/user/create-user/'

jQuery('#signup-form').submit( e => {
    e.preventDefault()
    jQuery.ajax({
        method: 'POST',
        data: jQuery('#signup-form').serialize(),
        url: signupEndpoint,
        success: response => {
            window.location.replace('/')
        },
        error: response => {
            console.log(response)
        }
    });
})