let updateUserEndpoint = apiEndpoint + '/user/update-user/'
let updateUserPassEndpoint = apiEndpoint + '/user/update-user-password/'

const preloadProfileData = () => {
    let firstName = localStorage.getItem('first_name')
    let lastName = localStorage.getItem('last_name')
    let email = localStorage.getItem('email')
    let birthday = localStorage.getItem('birthday')
    let vitalStatus = localStorage.getItem('vital_status')

    jQuery('#profile-first-name').val(firstName)
    jQuery('#profile-last-name').val(lastName)
    jQuery('#profile-email').val(email)
    jQuery('#profile-birthday').val(birthday)
    jQuery('#profile-vital-status').val(vitalStatus)
}

jQuery(document).ready(() => {
    preloadProfileData()

    jQuery('#profile-form').submit( e => {
        e.preventDefault()
        jQuery.ajax({
            method: 'POST',
            headers: {'Authorization': `Bearer ${localStorage.getItem('oauth_token')}`},
            data: jQuery('#profile-form').serialize(),
            url: updateUserEndpoint,
            success: response => {
                alert('User profile updated')
                localStorage.setItem('email', response.email)
                localStorage.setItem('first_name', response.first_name)
                localStorage.setItem('last_name', response.last_name)
                localStorage.setItem('birthday', response.birthday)
                localStorage.setItem('vital_status', response.vital_status)
                window.location.replace('/')
            },
            error: response => {
                alert(response.responseJSON.error)
            }
        });
    })

    jQuery('#change-password-form').submit( e => {
        e.preventDefault()
        jQuery.ajax({
            method: 'POST',
            headers: {'Authorization': `Bearer ${localStorage.getItem('oauth_token')}`},
            data: jQuery('#change-password-form').serialize(),
            url: updateUserPassEndpoint,
            success: response => {
                alert('Password updated!')
            },
            error: response => {
                alert(response.responseJSON.error)
            }
        });
    })
})