const jQuery = $

// CSRF JS implementation as suggested by django documentation
// https://docs.djangoproject.com/en/3.1/ref/csrf/
function getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        const cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i].trim();
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

const logoutUser = () => {
    localStorage.removeItem('username')
    localStorage.removeItem('oauth_token')
    localStorage.removeItem('email')
    localStorage.removeItem('first_name')
    localStorage.removeItem('last_name')
    window.location.replace('/')
}

jQuery(document).ready(() => {
    jQuery('.logout-button').click(logoutUser)
})