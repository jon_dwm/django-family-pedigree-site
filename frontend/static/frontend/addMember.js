let addMemberEndpoint = apiEndpoint + '/pedigree/create-member/'

jQuery(document).ready(() => {
    jQuery('#create-member-form').submit((e) => {
        e.preventDefault()
        jQuery.ajax({
            method: 'POST',
            headers: {'Authorization': `Bearer ${localStorage.getItem('oauth_token')}`},
            data: {
                'relation': jQuery('#relation').val(),
                'first_name': jQuery('#first-name').val(),
                'last_name': jQuery('#last-name').val(),
                'birthday': jQuery('#birthday').val(),
                'vital_status': jQuery('#vital-status').val(),
                'is_in_law': jQuery('#is-in-law').prop('checked'),
            },
            url: addMemberEndpoint,
            success: response => {
                alert('Member added')
            },
            error: function (response) {
                alert('Failed to add')
                console.log(response.responseJSON.errors)
            }
        });
    })
})