let loggedUser = localStorage.getItem('username')

const setHomepageView = () => {
    if(loggedUser){
        jQuery('#user-homepage').show()
        jQuery('#user-welcome').text(loggedUser)
    } else {
        jQuery('#no-user-homepage').show()
    }
}

jQuery(document).ready( e => {
        setHomepageView()
    }
)