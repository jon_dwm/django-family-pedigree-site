let getMemberApiEndpoint = apiEndpoint + '/pedigree/get-member/'
let updateMemberApiEndpoint = apiEndpoint + '/pedigree/update-member/'
let loadMemberDefaults = (response) => {
    jQuery('#relation').val(response.relation)
    jQuery('#update-member-form #first-name').val(response.first_name)
    jQuery('#update-member-form #last-name').val(response.last_name)
    jQuery('#update-member-form #birthday').val(response.birthday)
    jQuery('#update-member-form #vital-status').val(response.vital_status)
    jQuery('#update-member-form #is-in-law').attr('checked', response.is_in_law)
}

jQuery(document).ready(() => {
    jQuery.ajax({
		method: 'POST',
		headers: {'Authorization': `Bearer ${localStorage.getItem('oauth_token')}`},
        dataType: 'json',
        data: {'member_id': jQuery('#member_id').val()},
		url: getMemberApiEndpoint,
		success: response => {
            loadMemberDefaults(response[0])
		},
		error: function (response) {
            console.log(response)
		}
	});

    jQuery('#update-member-form').submit((e) => {
        e.preventDefault()
        jQuery.ajax({
            method: 'POST',
            headers: {'Authorization': `Bearer ${localStorage.getItem('oauth_token')}`},
            data: {
                'relation': jQuery('#relation').val(),
                'first_name': jQuery('#first-name').val(),
                'last_name': jQuery('#last-name').val(),
                'birthday': jQuery('#birthday').val(),
                'vital_status': jQuery('#vital-status').val(),
                'member_id': jQuery('#member-id').val(),
                'is_in_law': jQuery('#is-in-law').prop('checked'),
            },
            url: updateMemberApiEndpoint,
            success: response => {
                alert('Member updated')
            },
            error: function (response) {
                alert('Failed to update')
                console.log(response)
            }
        });
    })
})