let loginEndpoint = apiEndpoint + '/user/get-user/'

let generateToken = () => {
    let username = jQuery('#user-login-form #login_username').val()
    let password = jQuery('#user-login-form #login_password').val()

    jQuery.ajax({
        method: 'POST',
        data: jQuery('#user-login-form').serialize(),
        url: '/authenticate/',
        success: accessToken => {
            localStorage.setItem('oauth_token', accessToken)
            loginUser(accessToken)
        },
        error: response => {
            console.log(response)
        }
    });
}

let loginUser = (accessToken) => {
    jQuery.ajax({
        method: 'POST',
        headers: {'Authorization': `Bearer ${accessToken}`},
        data: jQuery('#user-login-form').serialize(),
        url: loginEndpoint,
        success: response => {
            localStorage.setItem('username', response.username)
            localStorage.setItem('email', response.email)
            localStorage.setItem('first_name', response.first_name)
            localStorage.setItem('last_name', response.last_name)
            localStorage.setItem('birthday', response.birthday)
            localStorage.setItem('vital_status', response.vital_status)
            window.location.replace('/')
        },
        error: response => {
            alert(response.responseJSON.error)
        }
    });
}

jQuery(document).ready(() => {
    jQuery('#user-login-form').submit( e => {
        e.preventDefault()
        generateToken()
    })
})