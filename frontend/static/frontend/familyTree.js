let deleteMemberApiEndpoint = apiEndpoint + '/pedigree/delete-member/'
let updateMemberApiEndpoint = apiEndpoint + '/pedigree/update-member/'
let getMemberApiEndpoint = apiEndpoint + '/pedigree/get-member/'

let username = localStorage.getItem('username')
let firstName = localStorage.getItem('first_name')
let lastName = localStorage.getItem('last_name')
let birthday = localStorage.getItem('birthday')
let vitalStatus = localStorage.getItem('vital_status')

let loadUserInfo = () => {
	jQuery('#user-first-name').text(firstName)
	jQuery('#user-last-name').text(lastName)
	jQuery('#user-birthday').text(birthday)
	jQuery('#user-vital-status').text(vitalStatus)
}

let populateFamilyMember = () => {
	jQuery.ajax({
		method: 'POST',
		headers: {'Authorization': `Bearer ${localStorage.getItem('oauth_token')}`},
		dataType: 'json',
		url: getMemberApiEndpoint,
		success: response => {
			response.forEach(member => {
				let inLawSuffix = member.is_in_law ? '-in-law' : ''
				jQuery('#family-member-table').append(`
					<tr class='member-row'>
						<td>
						${member.relation}${inLawSuffix}
						</td>
						<td>${member.first_name}</td>
						<td>${member.last_name}</td>
						<td>${member.birthday}</td>
						<td>${member.vital_status}</td>
						<td>
						<form action="/update-member/${member.member_id}" method="POST">
							<input type="hidden" name="csrfmiddlewaretoken" value="${getCookie('csrftoken')}">
							<input type="submit" value="Edit"/>
						</form>
						<form class="delete-member-form" method="POST">
							<input type="hidden" name="csrfmiddlewaretoken" value="${getCookie('csrftoken')}">
							<input type="hidden" id="member_${member.member_id}" name="member_id" value="${member.member_id}">
							<input type="submit" value="Delete"/>
						</form>
						</td>
					</tr>
				`)
			});

			jQuery('.delete-member-form').submit(e => {
				let memberToDelete = jQuery(e.target).closest('.delete-member-form')
				e.preventDefault()
				jQuery.ajax({
					method: 'POST',
					headers: {'Authorization': `Bearer ${localStorage.getItem('oauth_token')}`},
					data: memberToDelete.serialize(),
					dataType: 'json',
					url: deleteMemberApiEndpoint,
					success: response => {
						alert('Member deleted')
						jQuery('.member-row').remove()
						populateFamilyMember()
					},
					error: function (response) {
						alert('Failed to delete. Please try again')
					}
				});
			})
		},
		error: function (response) {
			console.log(response)
		}
	});
}

jQuery(document).ready(() => {
	populateFamilyMember()
	loadUserInfo()
})