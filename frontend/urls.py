from django.urls import path
from . import views

app_name = 'frontend'
urlpatterns = [
    path('', views.homepage, name='homepage'),
    path('login/', views.user_login, name='user_login'),
    path('signup/', views.user_signup, name='user_signup'),
    path('edit-profile/', views.edit_profile, name='edit_profile'),
    path('edit-password/', views.edit_password, name='edit_password'),
    path('family-tree/', views.family_tree_view, name='family_tree_view'),
    path('update-member/<int:member_id>', views.edit_member, name='edit_member'),
    path('add-member/', views.add_member, name='add_member'),
    path('authenticate/', views.authenticate, name='authenticate'),
]